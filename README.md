# SQL - DDL and DML

## First things first

This project is the result of a SQL course I took during my associate's degree.
Thanks to this experience I learnt:
- how relational data structures work and how to implement a programming logic on its characteristics
- SQL and MySQL documentation
- DB communication between Python and SQL
- Beekeper studio
- DDL and DML

## Description

This repository contains variou SQL related projects:
- DB creation
- Data Definition Language
- Data Manipulation Language
